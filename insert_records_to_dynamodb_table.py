import boto3

session = boto3.session.Session(region_name='us-east-1')

dynamodb = session.resource('dynamodb')

table = dynamodb.Table("TestTable")

data = 'x' * 100

for i in range(1, 11):
    for j in range(1, 11):
        print (i, j)
        table.put_item(
            Item={
                'pk': i,
                'sk': j,
                'data': {'S': data}
                }
        )

